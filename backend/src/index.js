const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

const server = require('http').Server(app);
const io = require('socket.io')(server);

mongoose.connect('mongodb://willian:willianGoweek2018@ds155293.mlab.com:55293/goweek-db', {
	useNewUrlParser: true
});

// Middleware
app.use((req, res, next) => {
	req.io = io;

	return next();
});

app.use(cors());
app.use(express.json());
app.use(require('./routes'));

/* app.get('/', (req, res) => {
	return res.send('Hello World');
}); */


/* app.listen(3000, () => {
	console.log('Server started on port 3000');
}); */
server.listen(3000, () => {
	console.log('Server started on port 3000');
});